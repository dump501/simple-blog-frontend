import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Api from '../Api';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  getPosts(){
    return this.http.get<any>(`${Api.apiRoot}/post`)
  }
}
