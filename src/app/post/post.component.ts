import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  posts: Object | undefined

  constructor(private postService: PostService){}

  ngOnInit(): void {
    this.postService.getPosts().subscribe(data => {
      this.posts = data;
      console.log(this.posts);

    })
  }

}
