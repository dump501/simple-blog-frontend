import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  serverStatus: Object | undefined;

  constructor(private _http: HttpService){

  }

  ngOnInit(){
    this._http.getServerStatus().subscribe(data => {
      this.serverStatus = data;
      console.log(this.serverStatus);

    })
  }

}
