import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import Api from './Api';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor( private http: HttpClient ) {

  }

  getServerStatus(){
    return this.http.get(`${Api.apiRoot}`)
  }
}
